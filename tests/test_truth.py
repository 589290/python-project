# SPDX-FileCopyrightText: © 2022 Jesse P. Johnson <johnson_jesse@bah.com>
# SPDX-License-Identifier: Unlicensed
"""assert truth"""

from src import main

def test_truth():
    ''' sample test function '''
    assert True

def test_message():
    ''' test api message '''
    assert main.api_message() == "Hello World! Behold my wonderful API!"
