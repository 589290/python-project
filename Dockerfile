FROM python:3.9-slim AS base

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

FROM base AS python-deps

# Install pipenv and compilation dependencies
RUN pip install --no-cache-dir pipenv==2023.2.4

# Install python dependencies in /.venv
WORKDIR /
COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM base AS runtime

# Copy virtual env from python-deps stage
COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

# for healthcheck
# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Create and switch to a new user
RUN useradd -d /app -u 1000 -s /bin/bash user
WORKDIR /app
USER user

# Install application into container
COPY ./ ./

# Expose the port the application will run on
EXPOSE 40404

# Run the executable
ENTRYPOINT ["/bin/bash", "-c", "python src/main.py --port 40404 --log-level info & sleep infinity & wait;"]

HEALTHCHECK --start-period=60s --retries=3 CMD curl --fail http://127.0.0.1:40404 || exit 1
