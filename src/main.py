"""main function for app."""

import uvicorn
from fastapi import FastAPI

my_awesome_api = FastAPI()


def api_message():
    """Provide greeting message."""
    return 'Hello World! Behold my wonderful API!'


@my_awesome_api.get('/')
async def root():
    """Returm a dict."""
    return {'message': api_message()}

if __name__ == '__main__':
    uvicorn.run(
        'main:my_awesome_api', host='0.0.0.0', port=40404, log_level='info'
    )
