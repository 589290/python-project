# nosemgrep
import requests

def test_api():
    response = requests.get("http://python-project:80")
    response_json = response.json()
    assert response.status_code == 200
    assert response_json["message"] == "Hello World! Behold my wonderful API!"